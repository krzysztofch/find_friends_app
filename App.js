import React, { Component } from 'react';
import { Text, View, Button, StyleSheet } from 'react-native';

import FetchLocation from './components/FetchLocation';
import UsersMaps from './components/UsersMap';

import mockLocations from './mockFriendLocations.js';
import './helpers.js';

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {    
      mapCentre: null,
      userLocation: null,
      friendsLocations: null, 
    };

    this.getUserLocationHandler = this.getUserLocationHandler.bind(this);
    this.findFriendsHandler = this.findFriendsHandler.bind(this);
    this.setMapCentre = this.setMapCentre.bind(this);
  }

  getUserLocationHandler() {
    navigator.geolocation.getCurrentPosition(position => {   
        this.setState({
          mapCentre: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude, 
            latitudeDelta: 0.04,
            longitudeDelta: 0.04,
          },
          userLocation: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude, 
          }
        });
      }, err => {
        console.log(err);
      }
    );
  };

  findFriendsHandler() {
    this.setState({
      friendsLocations: mockLocations
    }, () => {
      this.setMapCentre();
    });    
  }

  setMapCentre() {
    let latitudes = this.state.friendsLocations.map(item => item.coordinates.latitude);    
    let longitudes = this.state.friendsLocations.map(item => item.coordinates.longitude);
        
    if(userLocation){
      latitudes.push(this.state.userLocation.latitude);
      longitudes.push(this.state.userLocation.longitude);
    };

    const maxLatitude = latitudes.max();
    const minLatitude = latitudes.min();
    const latitudeCenter = (maxLatitude + minLatitude) / 2;
    const latitudeDelta = (maxLatitude - minLatitude) * 1.35;   
    
    const maxLongitude = longitudes.max();
    const minLongitude = longitudes.min();
    const longitudeCenter = (maxLongitude + minLongitude) / 2;
    const longitudeDelta = (maxLongitude - minLongitude) * 1.35;

    this.setState({
      mapCentre: {
        latitude: latitudeCenter,
        longitude: longitudeCenter,            
        latitudeDelta: latitudeDelta,
        longitudeDelta: longitudeDelta,
      }
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.section}>
          <Button title="Find your friends!" onPress={this.findFriendsHandler} />
        </View>
        <FetchLocation onGetLocation={this.getUserLocationHandler} /> 
        <UsersMaps  mapCentre={this.state.mapCentre}
                    userLocation={this.state.userLocation} 
                    friendsLocations={this.state.friendsLocations} />       
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  section: {
    marginTop: 25,
    marginBottom: 25
  }
});
