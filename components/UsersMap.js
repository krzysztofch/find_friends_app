import React from 'react';
import { View, StyleSheet } from 'react-native';
import MapView from 'react-native-maps';

const usersMap = props => {
    return (  
        <View style={styles.mapContainer}>
            <MapView initialRegion={{
                        latitude: 37.78825,
                        longitude: -122.4324,
                        latitudeDelta: 0.0422,
                        longitudeDelta: 0.0422,
                    }}
                    region={props.mapCentre}
                    style={styles.map} >

                {props.userLocation && <MapView.Marker title="user" coordinate={props.userLocation} />}

                {props.friendsLocations && props.friendsLocations.map(friend => (
                     <MapView.Marker key={friend.id} title={friend.id.toString()} coordinate={friend.coordinates} />   
                ))}
            </MapView>
        </View>
    );
};

const styles = StyleSheet.create({
    mapContainer: {
        width: '100%',
        height: 350,
        marginTop: 25
    },
    map: {
        width: '100%',
        height: '100%'
    }
})

export default usersMap;
