const mockLocations = [
    {
        id: 0,
        coordinates: {
            latitude: 50.029167,
            longitude: 22.01937,
        }        
    },
    {
        id: 1,
        coordinates: {
            latitude: 50.019167,
            longitude: 22.02937,
        } 
    },
    {
        id: 2,
        coordinates: {
            latitude: 50.039167,
            longitude: 22.019371,
        } 
    },
    {
        id: 3,
        coordinates: {
            latitude: 50.009167,
            longitude: 22.01937,
        } 
    },
    {
        id: 4,
        coordinates: {
            latitude: 50.009167,
            longitude: 22.00937,
        } 
    },
    {
        id: 5,
        coordinates: {
            latitude: 50.209167,
            longitude: 22.00937,
        } 
    },
    {
        id: 6,
        coordinates: {
            latitude: 50.009167,
            longitude: 22.20937,
        } 
    }

]

export default mockLocations;